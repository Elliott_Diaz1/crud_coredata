//
//  CDHelper.swift
//  CoreDataStack
//
//  Created by Elliott Diaz on 6/1/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.
//

import Foundation
import CoreData

class CDHelper
{
    
    static let sharedInstance = CDHelper()
    
    lazy var storesDirectory: NSURL = {
        let fileManager = NSFileManager.defaultManager()
        let urls = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var localStoreURL: NSURL = {
        let url = self.storesDirectory.URLByAppendingPathComponent("CoreDataStack.sqlite")
        return url
    }()
    
    lazy var modelURL: NSURL = {
        let bundle = NSBundle.mainBundle()
        if let url = bundle.URLForResource("Model", withExtension: "momd")
        {
            return url
        }
        else
        {
            print("Critical - Managed Object File Not Found")
            abort()
        }
    }()
    
    lazy var model: NSManagedObjectModel = {
        return NSManagedObjectModel(contentsOfURL: self.modelURL)!
    }()
    
    lazy var coordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.model)
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: self.localStoreURL, options: nil)
        } catch {
            print("Could not add persistent store")
            abort()
        }
        return coordinator
    }()
}

