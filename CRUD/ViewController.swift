//
//  ViewController.swift
//  CRUD
//  Copy - Read - Update - Delete
//
//  Created by Elliott Diaz on 6/1/16.
//  Copyright © 2016 Elliott Diaz. All rights reserved.

import UIKit
import CoreData

class ViewController: UIViewController {
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let managedObjectContext = NSManagedObjectContext( concurrencyType: .MainQueueConcurrencyType )
        managedObjectContext.persistentStoreCoordinator = CDHelper.sharedInstance.coordinator
        createPeople( managedObjectContext )
        updatePeople( managedObjectContext )
        deletePeople( managedObjectContext )
        fetchPeople ( managedObjectContext )
    }
    
    func createPeople( managedObjectContext: NSManagedObjectContext )
    {
        let names = ["Jane","John","Mary","Michael"]
        
        for n in names
        {
            guard let person = NSEntityDescription.insertNewObjectForEntityForName( "Person", inManagedObjectContext: managedObjectContext ) as? Person
                else { continue }
            person.name = n
        }
        
        do {
            print("Save Attempting...")
            try managedObjectContext.save()
        } catch {
            print( "Error Saving People" )
        }
    }
    
    func fetchPeople( managedObjectContext: NSManagedObjectContext ) -> [Person]?
    {
        let request = NSFetchRequest(entityName: "Person" )
        
        do
        {
            guard let people = try managedObjectContext.executeFetchRequest( request ) as? [Person] else { return nil }
            
            for p in people
            {
                print( p.name )
                print( p.age  )
            }
            return people
        }
        catch
        {
            print( "Error Fetching People" )
        }
        return nil
    }
    
    func updatePeople( managedObjectContext: NSManagedObjectContext )
    {
        guard let people = fetchPeople( managedObjectContext ) else { return }
        
        for p in people
        {
            p.age = 27
        }
        
        do
        {
            try managedObjectContext.save()
        }
        catch
        {
            print( "Error Updating People" )
        }
    }
    
    func deletePeople( managedObjectContext: NSManagedObjectContext )
    {
        guard let people = fetchPeople( managedObjectContext ) else { return }
        
        for p in people
        {
            managedObjectContext.deleteObject(p)
        }
        
        do
        {
            try managedObjectContext.save()
        }
        catch
        {
            print( "Error Deleting People" )
        }
    }
    
}






































